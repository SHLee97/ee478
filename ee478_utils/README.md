# ee478_utils


## Package structure
```bash
└── 📁ee478_utils
    └── 📁logger                # Logger scripts
    └── 📁tests                 # Tester to run your policy
        └── eval_freewalk.py
```
